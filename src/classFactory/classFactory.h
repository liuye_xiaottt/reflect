#ifndef _CLASSFACTORY_H_
#define _CLASSFACTORY_H_

#include "map"
#include "string"
#include "classObject.h"
#include "classField.h"
#include "classMethod.h"
using namespace std;

class ClassObject;

typedef ClassObject * (*createObjectFunction)();

class ClassFactory
{
public:
    ClassFactory();
    ~ClassFactory();

    ClassObject * getClassFromName(const string className);
    void registerClass(const string className, createObjectFunction function);
    void registerClassField(const string className, const string fieldName, size_t offset);
    ClassField * getClassFieldFromName(const string className);
    void registerClassMethod(const string & className, const string & methodName, size_t funcPtr);
    ClassMethod * getClassMethodFromName(const string & className);
    

private:
    map<string, createObjectFunction> m_classs;
    map<string, ClassField> m_classFields;
    map<string, ClassMethod> m_classMethods;
};



#endif