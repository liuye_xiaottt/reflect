#include "classFactory.h"


ClassFactory::ClassFactory()
{

}

ClassFactory::~ClassFactory()
{

}


ClassObject * ClassFactory::getClassFromName(const string className)
{
    auto it = m_classs.find(className);
    if(it == m_classs.end())
    {
        return nullptr;
    }
    ClassObject * obj = it->second();
    obj->m_className = className;
    return obj;
}

void ClassFactory::registerClass(const string className, createObjectFunction function)
{
    m_classs[className] = function;
}

void ClassFactory::registerClassField(const string className, const string fieldName, size_t offset)
{
    auto it = m_classFields.find(className);
    if(it == m_classFields.end())
    {
        ClassField classfield;
        classfield.setFieldNameOffset(fieldName, offset);
        m_classFields[className] = classfield;
        return;
    }
    m_classFields[className].setFieldNameOffset(fieldName, offset);
}

ClassField * ClassFactory::getClassFieldFromName(const string className)
{
    auto it = m_classFields.find(className);
    if(it == m_classFields.end())
    {
        return nullptr;
    }
    return &m_classFields[className];
}

void ClassFactory::registerClassMethod(const string & className, const string & methodName, size_t funcPtr)
{
    auto it = m_classMethods.find(className);
    if(it == m_classMethods.end())
    {
        ClassMethod classMethod;
        classMethod.regiserMethod(methodName, funcPtr);
        m_classMethods[className] = classMethod;
    }
    m_classMethods[className].regiserMethod(methodName, funcPtr);
}

ClassMethod * ClassFactory::getClassMethodFromName(const string & className)
{
    auto it = m_classMethods.find(className);
    if(it == m_classMethods.end())
    {
        return nullptr;
    }
    return &it->second;
}


