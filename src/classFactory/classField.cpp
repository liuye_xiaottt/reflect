#include "classField.h"


size_t ClassField::getOffsetFromFieldName(const string fieldName)
{
    auto it = m_fields.find(fieldName);
    if(it == m_fields.end())
    {
        return -1;
    }
    return it->second;
}

void ClassField::setFieldNameOffset(const string fieldName, size_t offset)
{
    m_fields[fieldName] = offset;
}



