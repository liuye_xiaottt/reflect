#include "classMethod.h"



void ClassMethod::regiserMethod(const string & methodName, size_t funcPtr)
{
    m_methods[methodName] = funcPtr;
}

size_t ClassMethod::getMethodPtrFromName(const string & methodName)
{
    auto it = m_methods.find(methodName);
    if(it == m_methods.end())
    {
        return -1;
    }
    return it->second;
}
