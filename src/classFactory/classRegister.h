#ifndef _CLASSREGISTER_H_
#define _CLASSREGISTER_H_

#include "classObject.h"
#include "classFactory.h"
#include "singleton.hpp"


class ClassRegister
{
public:
    ClassRegister(const string className, createObjectFunction func);
    ClassRegister(const string className, const string fieldName, size_t offset);
    ClassRegister(const string & className, const string methodName, size_t funcPtr, bool uesless);
};



#endif