#include "classRegister.h"



ClassRegister::ClassRegister(const string className, createObjectFunction func)
{
    Singleton<ClassFactory>::getInstance().registerClass(className, func);
}

ClassRegister::ClassRegister(const string className, const string fieldName, size_t offset)
{
    Singleton<ClassFactory>::getInstance().registerClassField(className, fieldName, offset);
}

ClassRegister::ClassRegister(const string & className, const string methodName, size_t funcPtr, bool uesless)
{
    Singleton<ClassFactory>::getInstance().registerClassMethod(className, methodName, funcPtr);
}

