#include "classObject.h"





void ClassObject::call(const string & methodName)
{
    ClassMethod * classMethod = Singleton<ClassFactory>::getInstance().getClassMethodFromName(m_className);
    size_t funcPtr = classMethod->getMethodPtrFromName(methodName);
    typedef function<void(decltype(this))> method;
    (*(method*)funcPtr)(this);
}





