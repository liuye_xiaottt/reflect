#include <iostream>
#include "classFactory.h"
#include "singleton.hpp"
#include "classRegister.h"
#include "functional"

class A : public ClassObject
{
public:
    A()
    {

    }
    ~A()
    {

    }

    void show()
    {
        cout << "A" << endl;
    }

    void show2()
    {
        cout << "A2" << endl;
    }

public:
    int m_age = 2;
    int m_name = 1;
    
};

RIGISTERCLASS(A)
RIGISTERCLASSFIELD(A, m_age)
RIGISTERCLASSFIELD(A, m_name)
RIGISTERCLASSMETHOD(A, show)
RIGISTERCLASSMETHOD(A, show2)

// ===========================================



int main(int, char**) {


    ClassObject * obj = Singleton<ClassFactory>::getInstance().getClassFromName("A");
    obj->set<int>("m_name", 12);
    int name;
    obj->get<int>("m_name", name);
    cout << name << endl;

    obj->call("show2");


}
