#ifndef _CLASSMETHOD_H_
#define _CLASSMETHOD_H_

#include "map"
#include "string"

using namespace std;

class ClassMethod
{
public:
    void regiserMethod(const string & methodName, size_t funcPtr);
    size_t getMethodPtrFromName(const string & methodName);


private:
    map<string, size_t> m_methods;

};




#endif