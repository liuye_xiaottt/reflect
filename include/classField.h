#ifndef _CLASSFIELD_H_
#define _CLASSFIELD_H_

#include "map"
#include "string"

using namespace std;

class ClassField
{
public:
    size_t getOffsetFromFieldName(const string fieldName);
    void setFieldNameOffset(const string fieldName, size_t offset);

private:
    map<string, size_t> m_fields;

};



#endif