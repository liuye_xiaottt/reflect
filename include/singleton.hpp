#ifndef _SINGLETON_HPP_
#define _SINGLETON_HPP_

template<class T>
class Singleton
{
public:
    static T & getInstance()
    {
        static T t;
        return t;
    } 
};


#endif